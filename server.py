import os
import aiohttp

from aiohttp import web
from urllib.parse import urljoin


class BitbucketHook:
    def __init__(self, url, job_name, username, password):
        self.url = url
        self.job_name = job_name
        self.auth = aiohttp.BasicAuth(username, password) if username else None
        self.client = aiohttp.ClientSession(auth=self.auth)

    async def get_crumb(self):
        url = urljoin(self.url, 'crumbIssuer/api/json')
        async with self.client.get(url) as resp:
            data = await resp.json()
        crumb = data['crumb']
        header = data['crumbRequestField']
        return {header: crumb}

    async def send_hook(self, data):
        crumb = await self.get_crumb()
        source = data['source']['branch']['name']
        dest = data['destination']['branch']['name']
        # replace api version from 2.0 to 1.0 because 2.0 read-only yet
        comment_api_endpoint = data['links']['comments']['href'].replace('/2.0/', '/1.0/')
        payload = {
            'SOURCE': source,
            'DESTINATION': dest,
            'COMMENT_API_URL': comment_api_endpoint,
        }
        url = urljoin(self.url, '/job/%s/buildWithParameters' % self.job_name)
        async with self.client.post(url, data=payload, headers=crumb) as resp:
            text = await resp.text()
        return text

    async def bitbucket_hook(self, request):
        data = await request.json()
        if not data.get('pullrequest'):
            return web.Response(status=400)
        text = await self.send_hook(data['pullrequest'])
        return web.Response(text=text)

    async def __call__(self, request):
        return await self.bitbucket_hook(request)


jenkins_url = os.environ.get('JENKINS_URL')
jenkins_username = os.environ.get('JENKINS_USERNAME')
jenkins_password = os.environ.get('JENKINS_PASSWORD')
jenkins_job_name = os.environ.get('JENKINS_JOB_NAME')

bitbucket_hook = BitbucketHook(jenkins_url, jenkins_job_name, jenkins_username, jenkins_password)
app = web.Application()
app.router.add_post('/bitbucket-hook/', bitbucket_hook)

web.run_app(app)
